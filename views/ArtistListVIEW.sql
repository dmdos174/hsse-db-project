-- VIEW of artists without ID

DROP VIEW artists;
CREATE TEMP VIEW artists AS
    SELECT
        artist_nm,
        artist_desc
    FROM music_application.artist;