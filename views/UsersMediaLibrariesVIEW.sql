-- Table of users with them track libraries

DROP MATERIALIZED VIEW users_with_tracks;
CREATE MATERIALIZED VIEW users_with_tracks AS
    SELECT
        "user".user_nm,
        playlist.playlist_title,
        track.track_title
    FROM music_application.user
        NATURAL JOIN music_application.playlist_x_user AS playlist_list
        NATURAL JOIN music_application.playlist AS playlist
        NATURAL JOIN music_application.playlist_x_track AS playlist_track_list
        NATURAL JOIN music_application.track AS track;