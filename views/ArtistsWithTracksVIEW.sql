-- Table with artists and all them tracks

DROP MATERIALIZED VIEW artists_with_tracks;
CREATE MATERIALIZED VIEW artists_with_tracks AS
    SELECT
        artist.artist_nm,
        album.album_title,
        track.track_title
    FROM music_application.artist
        NATURAL JOIN music_application.album_x_artist AS album_list
        NATURAL JOIN music_application.album AS album
        NATURAL JOIN music_application.album_x_track AS album_track_list
        NATURAL JOIN music_application.track AS track;

SELECT * FROM artists_with_tracks;