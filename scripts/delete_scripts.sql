-- Exit From Playlists
DELETE FROM music_application.playlist_x_user
WHERE user_id = 2 AND playlist_id = 1;

-- Album was banned by the **** reasons
BEGIN;
DELETE FROM music_application.album_x_artist
WHERE album_id = 2;

DELETE FROM music_application.album
WHERE album_id = 2;
COMMIT;

-- User Delete Account
BEGIN;
DELETE FROM music_application."user"
WHERE user_acc = 'DDDDRRRROOOOIIIIDDDD';
COMMIT;

-- Ban Wave
BEGIN;
DELETE FROM music_application."user"
WHERE user_nm ~ 'ban_word';
COMMIT;
