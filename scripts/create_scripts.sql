
CREATE SCHEMA IF NOT EXISTS music_application;

CREATE TABLE IF NOT EXISTS music_application.user (
    user_id BIGSERIAL PRIMARY KEY,
    user_nm VARCHAR(255) NOT NULL,
    user_snm VARCHAR(255) NOT NULL,
    user_acc VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS  music_application.playlist (
    playlist_id BIGSERIAL PRIMARY KEY,
    playlist_title VARCHAR(255) NOT NULL,
    playlist_type_txt VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS  music_application.track (
    track_id BIGSERIAL PRIMARY KEY,
    track_data BYTEA NOT NULL,
    track_title VARCHAR(255) NOT NULL,
    track_sec INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS  music_application.album (
    album_id BIGSERIAL PRIMARY KEY,
    album_title VARCHAR(255) NOT NULL,
    album_desc VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS  music_application.artist (
    artist_id BIGSERIAL PRIMARY KEY,
    artist_nm VARCHAR(255) NOT NULL,
    artist_desc VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS  music_application.playlist_x_user (
    user_id BIGSERIAL NOT NULL REFERENCES music_application.user ON DELETE CASCADE,
    playlist_id BIGSERIAL NOT NULL REFERENCES music_application.playlist ON DELETE CASCADE,
    is_owner_flg BOOLEAN NOT NULL,
    PRIMARY KEY (user_id, playlist_id),
    FOREIGN KEY (playlist_id)
    REFERENCES music_application.playlist (playlist_id),
    FOREIGN KEY (user_id)
    REFERENCES music_application.user (user_id)
);

CREATE TABLE IF NOT EXISTS  music_application.playlist_x_track (
    track_id BIGSERIAL NOT NULL REFERENCES music_application.track ON DELETE CASCADE,
    playlist_id BIGSERIAL NOT NULL REFERENCES music_application.playlist ON DELETE CASCADE,
    track_num INTEGER NOT NULL,
    PRIMARY KEY (track_id, playlist_id),
    FOREIGN KEY (playlist_id)
    REFERENCES music_application.playlist (playlist_id),
    FOREIGN KEY (track_id)
    REFERENCES music_application.track (track_id)
);

CREATE TABLE IF NOT EXISTS  music_application.album_x_track (
    track_id BIGSERIAL NOT NULL UNIQUE REFERENCES music_application.track ON DELETE CASCADE,
    album_id BIGSERIAL NOT NULL REFERENCES music_application.album ON DELETE CASCADE,
    track_num INTEGER NOT NULL,
    PRIMARY KEY (track_id, album_id),
    FOREIGN KEY (album_id)
    REFERENCES music_application.album (album_id),
    FOREIGN KEY (track_id)
    REFERENCES music_application.track (track_id)
);

CREATE TABLE IF NOT EXISTS  music_application.album_x_artist (
    artist_id BIGSERIAL NOT NULL,
    album_id BIGSERIAL NOT NULL,
    is_owner_flg BOOLEAN NOT NULL,
    PRIMARY KEY (artist_id, album_id),
    FOREIGN KEY (album_id)
    REFERENCES music_application.album (album_id),
    FOREIGN KEY (artist_id)
    REFERENCES music_application.artist (artist_id)
);
