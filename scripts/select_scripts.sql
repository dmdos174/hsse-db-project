-- Get Artists count of Albums
SELECT
    artist_nm,
    count(*) AS CountOfAlbums
FROM music_application.artist NATURAL JOIN music_application.album_x_artist
GROUP BY artist_id;

-- Get all tracks of artists
SELECT
    artist_nm,
    album.album_title AS album_title,
    track_list.track_num AS track_num,
    track.track_title
FROM music_application.artist
    NATURAL JOIN music_application.album_x_artist AS album_list
    NATURAL JOIN music_application.album AS album
    NATURAL JOIN music_application.album_x_track AS track_list
    NATURAL JOIN music_application.track AS track
ORDER BY artist_nm, album_title, track_num;

-- Get all tracks of one artist
SELECT
    artist_nm,
    album.album_title AS album_title,
    track_list.track_num AS track_num,
    track.track_title
FROM music_application.artist
    NATURAL JOIN music_application.album_x_artist AS album_list
    NATURAL JOIN music_application.album AS album
    NATURAL JOIN music_application.album_x_track AS track_list
    NATURAL JOIN music_application.track AS track
WHERE artist_nm = 'Лжедмитрий IV';

-- SELECT summary durations of tracks from albums, that continue not less then 120 seconds
SELECT DISTINCT
    album.album_title,
    sum(track.track_sec) over(PARTITION BY album_title) AS summary_duration
FROM music_application.artist
    NATURAL JOIN music_application.album_x_artist AS album_list
    NATURAL JOIN music_application.album AS album
    NATURAL JOIN music_application.album_x_track AS track_list
    NATURAL JOIN music_application.track AS track
WHERE track_sec >= 120
GROUP BY album_title, track_sec;

-- SELECT all users following playlist 'Рок'
SELECT
    "user".user_nm,
    playlist_list.is_owner_flg AS isOwner
FROM music_application.user
    NATURAL JOIN music_application.playlist_x_user AS playlist_list
    NATURAL JOIN music_application.playlist AS playlist
WHERE playlist_title = 'Рок';

-- Get all playlist ranked by popularity
SELECT DISTINCT
    playlist_title,
    count AS amount_of_followers,
    dense_rank() over(ORDER BY count DESC) AS rank
FROM (
    SELECT
        playlist.playlist_title,
        count(*) over (partition by playlist_title) as count
    FROM music_application.user
        NATURAL JOIN music_application.playlist_x_user AS playlist_list
        NATURAL JOIN music_application.playlist AS playlist
    GROUP BY playlist_title, user_nm
) as followers
ORDER BY rank;

-- SELECT a lot of DATA -- contains missing data
SELECT
    artist.artist_nm,
    album.album_title,
    track.track_title,
    playlist.playlist_title,
    "user".user_nm
FROM music_application.artist
    NATURAL LEFT JOIN music_application.album_x_artist AS album_list
    NATURAL LEFT JOIN music_application.album AS album
    NATURAL LEFT JOIN music_application.album_x_track AS album_track_list
    NATURAL LEFT JOIN music_application.track AS track
    NATURAL LEFT JOIN music_application.playlist_x_track AS playlist_track_list
    NATURAL LEFT JOIN music_application.playlist AS playlist
    NATURAL LEFT JOIN music_application.playlist_x_user AS playlist_list
    NATURAL LEFT JOIN music_application."user" AS "user";

-- SELECT all followers of artist 'Norimyxxxo'
SELECT DISTINCT
    artist.artist_nm,
    "user".user_nm
FROM music_application.artist
    NATURAL JOIN music_application.album_x_artist AS album_list
    NATURAL JOIN music_application.album AS album
    NATURAL JOIN music_application.album_x_track AS album_track_list
    NATURAL JOIN music_application.track AS track
    NATURAL JOIN music_application.playlist_x_track AS playlist_track_list
    NATURAL JOIN music_application.playlist AS playlist
    NATURAL JOIN music_application.playlist_x_user AS playlist_list
    NATURAL JOIN music_application."user" AS "user"
WHERE artist_nm = 'Norimyxxxo';