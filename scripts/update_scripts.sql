-- Hack
UPDATE music_application.album
SET album_desc = 'Все отписывайтесь, это скам!'
WHERE album_title SIMILAR TO 'К%';

-- Update Login
UPDATE music_application.user
SET user_nm = 'Нагибатор3000'
WHERE user_acc = 'DDDDRRRROOOOIIIIDDDD';

-- Update Artist Desk
UPDATE music_application.artist
SET artist_desc = 'Лучший батл-репер 2023 года'
WHERE artist_nm = 'Otto';

-- Change AccData
UPDATE music_application.user
SET user_acc = 'ivanctl'
WHERE user_acc = 'iv4nctl';

-- Update Album Data
UPDATE music_application.album
SET album_title = concat(album_title, ' - V2.0')
WHERE album_id IN (
    SELECT album.album_id FROM music_application.album NATURAL JOIN music_application.album_x_artist
    WHERE artist_id = 2
);

-- Give Rules to User for Playlist
UPDATE music_application.playlist_x_user
SET is_owner_flg = true
WHERE user_id = 2 AND playlist_id = 1;
