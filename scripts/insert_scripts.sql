-- Users
INSERT INTO music_application.user (user_nm, user_snm, user_acc) VALUES
('Dmitry', 'Dostovalov', 'BEllDH'),
('Kiril', 'Andreev', 'prostokir123'),
('Danil', 'Kramar', 'whodye'),
('Tima', 'Belov', 'Tefaier'),
('Tanya', 'Miheeva', 'tanyaamiheeva'),
('Svyatoslav', 'Sharov', 'svyatsharik'),
('Ivan', 'Chai', 'iv4nctl'),
('Maria', 'Lebedeva', 'Roachhhhhh'),
('David', 'Alexian', 'DDDDRRRROOOOIIIIDDDD'),
('Oleg', 'Hex', 'h4x4d')
ON CONFLICT DO NOTHING;

-- Artists
INSERT INTO music_application.artist (artist_nm, artist_desc) VALUES
('Norimyxxxo', 'Norymixxxo оказался в числе артистов, которые "перевернули игру". С его подачи это выражение широко распространилось в рэп-среде.'),
('Лжедмитрий IV', 'Исполнитель, композитор, участник площадок RBL и Кубок МЦ'),
('Баста', 'Благодаря Басте русский хип-хоп покорил стадионы: в апреле 2015-го он стал первым рэпером, собравшим "Олимпийский".'),
('Мумий Тролль', 'Группа Ильи Лагутенко, радикально изменившая вектор развития русскоязычной рок-музыки, была основана осенью 1983 года и тогда носила название "Муми Тролль".'),
('Niletto', 'Автор романтических поп-хитов, ставший звездой после выхода трека "Любимка".'),
('Клава Кока', 'Целеустремлённая певица, блогер, ведущая с дерзкими поп-хитами и громкими дуэтами.'),
('Каста', 'Появившись в Ростове-на-Дону в 90-е годы, группа "Каста" создала новое место силы в русском хип-хопе. Родной для артистов город стал первым региональным рэп-центром со своим вайбом.'),
('Егор Крид', 'От пацанского рэпа к романтическим поп-хитам и обратно к респекту улиц - таков путь, проделанный Егором Кридом.'),
('Otto', 'Баттл-МС и рэп-исполнитель из Санкт-Петербурга, финалист 140 BPM Cup (3 сезон)'),
('Дима Билан', 'Дима Билан уже более 20 лет не покидает высшую лигу поп-сцены. На его счету немало ярких достижений: альбомы и синглы на верхних местах хит-парадов.')
ON CONFLICT DO NOTHING;

-- Albums
BEGIN;
INSERT INTO music_application.album (album_title, album_desc) VALUES
('Горгород', 'Концептуальный альбом'),
('Вечный жид', ' - я умру, но в этой песне буду вечно жить!'),
('Красота и уродство', 'Возвращение легенды спустя года'),
('miXXXtape II', 'Сборник песен 2012 - 2013')
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_artist (artist_id, album_id, is_owner_flg) VALUES
(1, 1, true),
(1, 2, true),
(1, 3, true),
(1, 4, true)
ON CONFLICT DO NOTHING;
COMMIT;

BEGIN;
INSERT INTO music_application.album (album_title, album_desc) VALUES
('Баста 4', '4 порядковый'),
('Сансара', ' ... голосами!')
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_artist (artist_id, album_id, is_owner_flg) VALUES
(3, 5, true),
(3, 6, true)
ON CONFLICT DO NOTHING;
COMMIT;

BEGIN;
INSERT INTO music_application.album (album_title, album_desc) VALUES
('Краш', 'Краш'),
('Покинула чат', 'Покинула чат')
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_artist (artist_id, album_id, is_owner_flg) VALUES
(5, 7, true),
(5, 8, true),
(6, 7, false),
(6, 8, false)
ON CONFLICT DO NOTHING;
COMMIT;

BEGIN;
INSERT INTO music_application.album (album_title, album_desc) VALUES
('Тёмный театр С.Г.В.М.', 'Микстейп'),
('Анхель', 'Танец архангелов'),
('Некрооазис', 'ЛД === АД')
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_artist (artist_id, album_id, is_owner_flg) VALUES
(2, 9, true),
(2, 10, true),
(2, 11, true)
ON CONFLICT DO NOTHING;
COMMIT;


-- Tracks
BEGIN;
INSERT INTO music_application.track (track_data, track_title, track_sec) VALUES
('---', 'Красота и уродство', 159),
('---', 'Чувствую', 215),
('---', 'Волапюк', 102),
('---', 'XXX SHOP', 55)
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_track (track_id, album_id, track_num) VALUES
(1, 3, 1),
(2, 3, 2),
(3, 4, 1),
(4, 4, 2)
ON CONFLICT DO NOTHING;
COMMIT;

BEGIN;
INSERT INTO music_application.track(track_data, track_title, track_sec) VALUES
('---', 'Краш', 171),
('---', 'Покинула чат', 199)
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_track (track_id, album_id, track_num) VALUES
(5, 7, 1),
(6, 8, 1)
ON CONFLICT DO NOTHING;
COMMIT;

BEGIN;
INSERT INTO music_application.track(track_data, track_title, track_sec) VALUES
('---', 'Пешки не ходят назад', 90),
('---', 'Дурной поступок', 271),
('---', 'Огонь по...', 327),
('---', 'Охрипшая тишина', 173),
('---', 'Шаман', 198)
ON CONFLICT DO NOTHING;

INSERT INTO music_application.album_x_track (track_id, album_id, track_num) VALUES
(7, 9, 1),
(8, 9, 2),
(9, 9, 3),
(10, 9, 4),
(11, 9, 5)
ON CONFLICT DO NOTHING;
COMMIT;

-- Playlists
BEGIN;
INSERT INTO music_application.playlist (playlist_title, playlist_type_txt) VALUES
('Солянка', 'public'),
('Реп', 'public'),
('ЛД', 'private'),
('Личное', 'private'),
('Каста', 'private'),
('Рок', 'public'),
('Классика', 'private'),
('索良卡', 'private'),
('سولانڪا', 'private'),
('Классика', 'public')
ON CONFLICT DO NOTHING;

INSERT INTO music_application.playlist_x_user (user_id, playlist_id, is_owner_flg) VALUES
(1, 1, true),
(2, 1, false),
(3, 1, false),
(1, 2, true),
(1, 3, true),
(2, 4, true),
(5, 6, true),
(4, 1, true),
(7, 6, false),
(6, 6, false),
(8, 6, false),
(8, 8, true)
ON CONFLICT DO NOTHING;

INSERT INTO music_application.playlist_x_track (track_id, playlist_id, track_num) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(5, 1, 4),
(6, 1, 5),
(7, 1, 6),

(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(7, 2, 5),
(8, 2, 6),
(9, 2, 7),

(7, 3, 1),
(8, 3, 2),
(9, 3, 3),
(10, 3, 4),
(11, 3, 5),

(1, 4, 1),
(2, 4, 2),
(5, 4, 3),
(6, 4, 4)
ON CONFLICT DO NOTHING;
COMMIT;