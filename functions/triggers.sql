CREATE OR REPLACE FUNCTION delete_only_phantom_playlist() RETURNS TRIGGER LANGUAGE plpgsql AS $$
    BEGIN
        IF (SELECT count(*) FROM music_application.playlist_x_track WHERE playlist_id = OLD.playlist_id) = 0 THEN
            DELETE FROM music_application.playlist WHERE playlist_id = OLD.playlist_id;
        END IF;
        RETURN NULL;
    END;
$$;

CREATE OR REPLACE TRIGGER clear_phantom_playlist
    AFTER UPDATE OR DELETE ON music_application.playlist_x_user FOR EACH ROW
    EXECUTE FUNCTION delete_only_phantom_playlist();

DELETE FROM music_application.playlist_x_user WHERE playlist_id = 2;