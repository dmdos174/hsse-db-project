-- Get summary time of tracks of artist
CREATE FUNCTION tracks_sum_duration(text)
    RETURNS TABLE("artist" text, "duration" bigint)
    LANGUAGE sql
AS
$$
    SELECT
        artists.artist_nm,
        sum(track.track_sec)
    FROM artists_with_tracks as artists
        NATURAL JOIN music_application.track
    WHERE artists.artist_nm = $1
    GROUP BY artists.artist_nm;
$$;

-- Get times of 'Norimyxxxo';
SELECT * FROM tracks_sum_duration('Norimyxxxo');

-- Get times of 'Лжедмитрий IV';
SELECT * FROM tracks_sum_duration('Лжедмитрий IV');

CREATE FUNCTION getTrackId(trackTile TEXT)
    RETURNS BIGINT
AS
$$
    SELECT track_id::BIGINT
        FROM music_application.track
        WHERE track_title = trackTile;
$$ LANGUAGE sql;

CREATE FUNCTION getAlbumId(albumtitle TEXT)
    RETURNS BIGINT
AS
$$
    SELECT album_id
        FROM music_application.album
        WHERE album_title = albumtitle;
$$ LANGUAGE sql;

CREATE FUNCTION getArtistId(name TEXT)
    RETURNS BIGINT
AS
$$
    SELECT artist_id
        FROM music_application.artist
        WHERE artist_nm = name;
$$ LANGUAGE sql;

-- Add new Track of Artist Into Album, If Album not exist then create new with this track
CREATE PROCEDURE add_track(artistname TEXT, albumtitle TEXT, title TEXT, data BYTEA) LANGUAGE plpgsql AS $$
    BEGIN
        IF (SELECT count(*) FROM music_application.album WHERE album_title = albumtitle) = 0 THEN
            INSERT INTO music_application.album VALUES(albumtitle, '---');

            INSERT INTO music_application.album_x_artist VALUES(getArtistId(artistname), getAlbumId(albumtitle), true);
        END IF;

        INSERT INTO music_application.track (track_data, track_title, track_sec) VALUES (data, title, 0);

        INSERT INTO music_application.album_x_track VALUES
        (
            getTrackId(title),
            getAlbumId(albumtitle),
            (SELECT count(*) AS cnt FROM music_application.album_x_track WHERE album_id = getAlbumId(albumtitle))
        );
    END;
$$;

CALL add_track('Norimyxxxo', 'Красота и уродство', 'Рашн Роуд Рейдж', '---');